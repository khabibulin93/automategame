"""
    TODO:
     1. Extend numder of accounts
     2. Implement text search for create and delete chars on account
-----3. Re-build programm for single use on each accounts
     4. Think about automate almost anything

"""

import os, sys
import time
import threading
import queue
import random
import run
from pathlib import Path
import customInput
import customFunctions
from lib.AutoHotPy import AutoHotPy

os.chdir(os.path.dirname(os.path.abspath(__file__)))

MinSimilarity = 0.6


    

class Launcher:

    def __init__(self):
        self.autohot_py = AutoHotPy()
        self.autohot_py.registerExit(self.autohot_py.ESC, self.stop_bot_event_handler)
        # init bot stop event
        self.bot_thread_stop_event = threading.Event()
        
        # init threads
        self.auto_py_thread = threading.Thread(target=self.start_auto_py, args=(self.autohot_py,))
        #self.bot_thread = threading.Thread(target=self.start_bot, args=(auto_py, self.bot_thread_stop_event, character_class))

        # start threads
        self.auto_py_thread.start()
        #self.bot_thread.start()
        self.customInput = customInput.CustomInput(self.autohot_py)
    
    @staticmethod
    def start_auto_py(auto):
        #start AutoHotPy
        auto.start()

    @staticmethod
    def stop_bot_event_handler(auto, event):
       # exit the program when you press ESC
        auto.stop()
    
    def runMainLoop(self):
        while True:
            print("""
            What to do ?
            1 - refine
            2 - meat
            3 - launch <Name>
            4 - launchAll
            5 - exit
            """)
            answer = input ()
            if answer == 'refine' :
                while True:
                    if self.autohot_py.F7.isPressed():
                        break
                    self.autoRefiner()
            elif answer == 'meat' :
                time.sleep(3)
                while True:
                    if self.autohot_py.F7.isPressed():
                        break
                    elif customFunctions.findImageExample(str(Path("Data\\InSideGame", "meat2.jpg")), .6):
                        break
                    self.meatClicker()
            elif answer == 'launch':
                self.launchGame(sys.argv[1])
            elif answer == 'launchAll':
                self.launchGame()
            elif answer == 'exit':
                sys.exit(0)
                break
            else:
                continue

    def launchGame(self, name):
        client = run.RunROM()
        client.runLauncher()

    def autoRefiner(self):
        self.customInput.mouseClick()
        time.sleep(random.randrange(.5, 3))
        self.customInput.mouseClick()
        time.sleep(random.randrange(3.1, 4.2))
    
    def meatClicker(self):
        self.customInput.mouseClick()
        time.sleep(random.uniform(3.4, 4.2))


if __name__ == "__main__":
    launcher = Launcher()
    launcher.runMainLoop()

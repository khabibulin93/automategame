from lib.InterceptionWrapper import InterceptionMouseState,InterceptionMouseStroke
import time, random
import win32gui

class CustomInput:
    def __init__(self, autohotpy):
        self.autohotpy = autohotpy
        self.A = self.autohotpy.A#.press()
        self.B = self.autohotpy.B#.press()
        self.C = self.autohotpy.C#.press()
        self.D = self.autohotpy.D#.press()
        self.E = self.autohotpy.E#.press()
        self.F = self.autohotpy.F#.press()
        self.G = self.autohotpy.G#.press()
        self.H = self.autohotpy.H#.press()
        self.I = self.autohotpy.I#.press()
        self.J = self.autohotpy.J#.press()
        self.K = self.autohotpy.K#.press()
        self.L = self.autohotpy.L#.press()
        self.M = self.autohotpy.M#.press()
        self.N = self.autohotpy.N#.press()
        self.O = self.autohotpy.O#.press()
        self.P = self.autohotpy.P#.press()
        self.Q = self.autohotpy.Q#.press()
        self.R = self.autohotpy.R#.press()
        self.S = self.autohotpy.S#.press()
        self.T = self.autohotpy.T#.press()
        self.U = self.autohotpy.U#.press()
        self.V = self.autohotpy.V#.press()
        self.W = self.autohotpy.W#.press()
        self.X = self.autohotpy.X#.press()
        self.Y = self.autohotpy.Y#.press()
        self.Z = self.autohotpy.Z#.press()
        self.N1 = self.autohotpy.N1#.press()
        self.N2 = self.autohotpy.N2#.press()
        self.N3 = self.autohotpy.N3#.press()
        self.N4 = self.autohotpy.N4#.press()
        self.N5 = self.autohotpy.N5#.press()
        self.N6 = self.autohotpy.N6#.press()
        self.N7 = self.autohotpy.N7#.press()
        self.N8 = self.autohotpy.N8#.press()
        self.N9 = self.autohotpy.N9#.press()
        self.N0 = self.autohotpy.N0#.press()
        self.COMMA = self.autohotpy.COMMA#.press()
        self.DOT = self.autohotpy.DOT#.press()
        self.SPACE = self.autohotpy.SPACE#.press()
        self.LEFT_SHIFT = self.autohotpy.LEFT_SHIFT#.press()
        self.ENTER = self.autohotpy.ENTER
        self.ALT_ENTER = self.autohotpy.ALT_ENTER_NUM
        #self. = self.autohotpy..press()
        self.dict = {"A": self.A, "B": self.B, "C": self.C, "D": self.D, "E": self.E, "F": self.F,
            "G": self.G, "H": self.H, "I": self.I, "J": self.J, "K": self.K,"L": self.L, "M": self.M,
            "N": self.N,"O": self.O, "P": self.P, "Q": self.Q, "R": self.R, "S": self.S, "T": self.T,
            "U": self.U, "X": self.X, "Y": self.Y, "Z": self.Z,"1": self.N1, "2": self.N2, "3": self.N3,
            "4": self.N4, "5": self.N5, "6": self.N6, "7": self.N7, "8": self.N8, "9": self.N9,
            "0": self.N0, "shift": self.LEFT_SHIFT, ".": self.DOT, ",": self.COMMA, "spc": self.SPACE,
            "ENTER": self.ENTER, "ALT_ENTER": self.ALT_ENTER,
        }
    
     # Brazenhem algo
    def draw_line(self, x1=0, y1=0, x2=0, y2=0):

        coordinates = []

        dx = x2 - x1
        dy = y2 - y1

        sign_x = 1 if dx > 0 else -1 if dx < 0 else 0
        sign_y = 1 if dy > 0 else -1 if dy < 0 else 0

        if dx < 0:
            dx = -dx
        if dy < 0:
            dy = -dy

        if dx > dy:
            pdx, pdy = sign_x, 0
            es, el = dy, dx
        else:
            pdx, pdy = 0, sign_y
            es, el = dx, dy

        x, y = x1, y1

        error, t = el / 2, 0

        coordinates.append([x, y])

        while t < el:
            error -= es
            if error < 0:
                error += el
                x += sign_x
                y += sign_y
            else:
                x += pdx
                y += pdy
            t += 1
            coordinates.append([x, y])

        return coordinates


    def mouseClick(self):
        """
        This function simulates a left click
        """
        stroke = InterceptionMouseStroke()
        stroke.state = InterceptionMouseState.INTERCEPTION_MOUSE_LEFT_BUTTON_DOWN
        self.autohotpy.sendToDefaultMouse(stroke)
        time.sleep(((random.randrange(1, 4, 1)/10)))
        stroke.state = InterceptionMouseState.INTERCEPTION_MOUSE_LEFT_BUTTON_UP
        self.autohotpy.sendToDefaultMouse(stroke)

    def mouseMove(self, center_image):
        initial_position = self.autohotpy.getMousePosition()
        coordinates = self.draw_line(initial_position[0], initial_position[1], center_image[0], center_image[1])
        x = 0
        for dot in coordinates:
            x += 1
            if x % 2 == 0 and x % 3 == 0:
                time.sleep(0.01)
            self.autohotpy.moveMouseToPosition(dot[0], dot[1])

   
    def typing(self, string):
        for k in string:
            if k.islower():
                s = k.upper()
                self.dict[s].press()
            elif k.isupper():
                self.LEFT_SHIFT.down()
                time.sleep(((random.randrange(1, 8, 1)/10)))
                self.dict[k].press()
                self.LEFT_SHIFT.up()
            else:
                if k in self.dict:
                    self.dict[k].press()
            time.sleep(((random.randrange(1, 8, 1)/10)))
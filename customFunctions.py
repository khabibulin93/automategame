import cv2, os, time, psutil
from pathlib import Path
from PIL import ImageOps, Image, ImageGrab
import numpy as np


def findImageExample(img, similarity):
        screen = ImageGrab.grab()
        screen_array = np.array(screen.getdata(), dtype=np.uint8).reshape((screen.size[1], screen.size[0], 3))
        screen_gray = cv2.cvtColor(screen_array, cv2.COLOR_BGR2GRAY)
        target = cv2.imread(img, 0)
        target_w, target_h = target.shape[::-1]
        res = cv2.matchTemplate(screen_gray, target, cv2.TM_CCOEFF_NORMED)
        if res.any():
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
            if max_val >= similarity:
                print("max-loc" + str(max_loc))
                print(max_val)
                print("center-target: {0}, {1}" .format(target_w, target_h))
                return [True, max_loc, target_w, target_h]
            else:
                print(max_val)
                return False
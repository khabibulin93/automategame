"""
TODO:
    1. Rewrite in to threading programm (use bot for Lineage2)
    2. separate function for different files

"""
import cv2, os, time, psutil
import win32gui, win32con, win32process
from pathlib import Path
from PIL import ImageOps, Image, ImageGrab
import numpy as np
import random
import customFunctions
import webbrowser 
import threading
import customInput

os.chdir(os.path.dirname(os.path.abspath(__file__)))

class RunROM:
    
    def __init__(self, account, autohotpy):
        self.account = account
        self.defaultPath = os.path.dirname(os.path.abspath(__file__))
        self.path_preSelected = self.defaultPath + '/Data/Autorization/LauncherPreSelected'
        self.path_autorization = self.defaultPath + '/Data/Autorization'
        self.path_namespace = self.defaultPath + '/Data/Autorization/NameSpace'
        self.nameSpace = {'Nasredin': 'Nasredin.png', 'Nasredin1': 'Nasredin1.png', 'Nasredin2': 'Nasredin2.png', 
        'Nasredin3': 'Nasredin3.png', 'Nasredin4': 'Nasredin4.png', 'Nasredin5': 'Nasredin5.png', 'Nasredin6': 'Nasredin6.png', 
        'Nasredin7': 'Nasredin7.png', 'Nasredin8': 'Nasredin8.png', 'Nasredin9': 'Nasredin9.png', 'Nasredin10': 'Nasredin10.png', 
        'Nasredin11': 'Nasredin11.png',}
        self.lastMaxLoc = customFunctions.findImageExample[1]
        self.target_w = customFunctions.findImageExample[2]
        self.target_h = customFunctions.findImageExample[3]
        self.autohot_py = autohotpy
        #self.autohot_py = AutoHotPy()
        #self.autohot_py.registerExit(self.autohot_py.ESC, self.stop_bot_event_handler)

        # init bot stop event
        #self.bot_thread_stop_event = threading.Event()

        # init threads
        #self.auto_py_thread = threading.Thread(target=self.start_auto_py, args=(self.autohot_py,))
        #self.bot_thread = threading.Thread(target=self.start_bot, args=(auto_py, self.bot_thread_stop_event, character_class))

        # start threads
        #self.auto_py_thread.start()
        #self.bot_thread.start()

    @staticmethod
    def start_auto_py(auto):
        """
        start AutoHotPy
        """
        auto.start()

    @staticmethod
    def stop_bot_event_handler(auto, event):
        """
        exit the program when you press ESC
        """
        auto.stop()

    """
    def l(logStr):
        logging.info(logStr)
    """
    
    def findProgram(self):
        for state in psutil.pids():
            try:
                name = psutil.Process(state).name()
            except psutil.NoSuchProcess:
                continue
            if name == "gfclient.exe":
                return state
        return None

    def enumWindowCallback(self, hwnd, pid):
        tid, current_pid = win32process.GetWindowThreadProcessId(hwnd)
        if pid == current_pid and win32gui.IsWindowVisible(hwnd):
            win32gui.SetForegroundWindow(hwnd)
            #l("window activated")

    def activegamewindow(self):
        gamepid = self.findProgram()
        print(gamepid)
        if gamepid is not None:
            win32gui.EnumWindows(self.enumWindowCallback, gamepid)
            
    def activeWindowName(self):
        hwnd = win32gui.GetForegroundWindow()
        tid, current_pid = win32process.GetWindowThreadProcessId(hwnd)
        return psutil.Process(current_pid).name()

    
    def findCenter (self):
        """
        
        Rewrite with using on tuple
        https://bit.ly/33xoHdp
        
        """
        center_x = self.lastMaxLoc[0] + (self.target_w / 2)
        center_y = self.lastMaxLoc[1] + (self.target_h / 2)

        return center_x, center_y


    def runLauncher(self):
        custom_input = kbd.CustomInput(self.autohot_py)
        if self.findProgram():
            print("Find Launcher")
            webbrowser.open("gfclient://")
            time.sleep(6)
            self.activegamewindow()
        else:
            print("Open Launcher")
            webbrowser.open("gfclient://")
            time.sleep(6)
            self.activegamewindow()
            
        time.sleep(1)
        if self.findImageExample(str(Path(self.path_preSelected, self.nameSpace[self.account])), .8):
            print("PreSelected")
            if self.findImageExample(str(Path(self.path_autorization, "playButton.png")), .8):
                print ("center" + str(self.findCenter()))
                custom_input.mouseMove(self.findCenter())
                custom_input.mouseClick()
        elif self.findImageExample(str(Path(self.path_autorization, "changerAccounts.png")), .8) :
            print("Open Changer")
            if self.findImageExample(str(Path(self.path_autorization, "showMore.png")), .8):
                custom_input.mouseMove(self.findCenter())
                custom_input.mouseClick()
            if self.findImageExample(str(Path(self.path_namespace, self.nameSpace[self.account])), .95):
                custom_input.mouseMove(self.findCenter())
                custom_input.mouseClick()
            if self.findImageExample(str(Path(self.path_autorization, "playButton.png")), .8):
                custom_input.mouseMove(self.findCenter())
                custom_input.mouseClick()

        else:
            print("Usual choose")
            if self.findImageExample(str(Path(self.path_autorization, "changeAccount.png")), .8):
                custom_input.mouseMove(self.findCenter())
                custom_input.mouseClick()
            if self.findImageExample(str(Path(self.path_autorization, "changeAccount2.png")), .8):
                custom_input.mouseMove(self.findCenter())
                custom_input.mouseClick()
            if self.findImageExample(str(Path(self.path_autorization, "showMore.png")), .8):
                custom_input.mouseMove(self.findCenter())
                custom_input.mouseClick()
            if self.findImageExample(str(Path(self.path_namespace, self.nameSpace[self.account])), .95):
                custom_input.mouseMove(self.findCenter())
                custom_input.mouseClick()
            if self.findImageExample(str(Path(self.path_autorization, "playButton.png")), .8):
                custom_input.mouseMove(self.findCenter())
                custom_input.mouseClick()
        
        counter = 0
        while True:
            time.sleep(1)
            counter += 1
            if self.findImageExample(str(Path(self.path_autorization, "target.png")), .8) :
                print("find")
                custom_input.typing('3991872AlekS')
                time.sleep(.3)
                custom_input.typing('ENTER')
                break
            print("Nothin times: " + str(counter))
        
if __name__ == "__main__":
    run = RunROM("Nasredin")
    run.runLauncher()